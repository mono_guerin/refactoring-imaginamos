<?php

public function post_confirm(){
	$id = Input::get('service_id');
	$servicio = Service::find($id);
	if(isset($servicio))
	{
		if(strcmp($servicio->status_id , '6') == 0)
		{
			return Response::json(array('error' => '2'));
		}
		if(is_null($servicio->driver_id) && strcmp($servicio->status_id , '1') == 0)
		{
			//Comentario de lo que se hace aca o valor de la variable servicio
			$servicio = Service::update($id, array(
				'driver_id' => Input::get('driver_id'),
				'status_id' => '2'
			));
			//Comentario de lo que se esta actualizando aca
			Driver::update(Input::get('driver_id'),array(
				"available" => '0'
			));
			//Comentario de lo que se hace aca
			$driverTmp = Driver::find(Input::get('driver_id'));
			Service::update($id, array(
				'car_id' => $driverTmp->car_id
			));
			//Notificar al usuario
			$pushMessage = 'Tu servicio ha sido confirmado!';
			$servicio = Service::find($id);
			$push = Push::make();
			//Validacion de uuid vacio para retornar error 0
			if(empty($servicio->user->uuid)){
				return Response::json(array('error' => '0'));
			}
			//Iphone Push Message if user->type = 1 if not so it's Android Push
			if(strcmp($servicio->user->type, '1') == 0){
				$result = $push->ios($servicio->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $servicio->id));
			} else {
				$result = $push->android2($servicio->user->uuid, $pushMessage, 1, 'default', 'Open', array('service_id' => $servicio->id));
			}
			return Response::json(array('error' => '0'));
		} 
		else
		{
			return Response::json(array('error' => '1'));
		}
	} 
	else 
	{
		return Response::json(array('error' => '3'));
	}
}